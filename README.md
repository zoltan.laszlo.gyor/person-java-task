# Person Java Task

1. Hozzunk létre egy nemek ábrázolásához használt Gender nevű enumot! Ebben szerepeljen két érték, amelyek rendre Gender.MALE (férfi) és Gender.FEMALE (nő).

2. Készítsünk Person névvel egy olyan osztályt, amelyben személyi adatokat tudunk eltárolni! A rögzíteni kívánt adatok: a személy vezeték- és keresztneve (mind a kettő String), neme (Gender), születési éve (int).

3. Legyen a Person osztálynak egy olyan statikus metódusa makePerson() névvel, amely ezeket az adatokat paraméterként bekéri és ezekből összeállít egy Person típusú objektumot!

  A létrehozás előtt azonban ellenőrizzük, hogy minden megadott paraméter eleget tesz a személyi adatokra érvényes megszorításokra:

  A vezeték- és keresztnév nem lehet üres, valamint legalább kétkarakteresnek kell lennie és nagybetűvel kell kezdődnie. Ezek kifejezéséhez nyugodtan használjuk a java.lang.String osztályt!

  A születési évnek 1880 és 2019 között kell lennie.

  Amennyiben a megadott paraméterek nem tesznek eleget a fenti megszorításoknak, úgy a metódus adjon vissza üres, vagyis null referenciát! (Vagyis ilyenkor objektum nem jön létre.)

4. Egészítsük ki a Person osztályt egy show() metódussal, amely String típusú értékké alakítja az adott objektum belső állapotát!

5. Adjunk a Person osztályhoz egy isAdult() metódust, amely egy boolean típusú értékkel tér vissza, és attól függően igaz (true) vagy hamis (false), hogy az illető személy 18 évesnél idősebb vagy sem!

6. Készítsünk egy equals() nevű metódust a Person osztályhoz! Ennek az a feladata, hogy eldöntse a paraméterként megadott másik Person objektumról, hogy megegyezik-e az aktuális példánnyal. Ennek megfelelően a visszatérési értéke egy boolean lesz, amely igaz, ha megegyezik, ellenben hamis.

  Vigyázzunk arra, hogy mivel referenciát adunk át paraméterként, az lehet (többnyire véletlenül) null érték is! Ilyenkor értelemszerűen az eredménye hamis lesz.

7. Tegyük az eddigi osztályokat a person csomagba és készítsünk hozzá egy főprogramot, amelyben szabványos bemenetről bekérve létrehozunk egy Person objektumot és kiírjunk a (show() metódussal) a szabványos kimenetre! A főprogram kerüljön a main csomagba!
