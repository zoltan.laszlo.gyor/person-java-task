package java_exercises2;

import java.util.Calendar;

import person.Gender;
import person.Person;

public class Main {
	public static void main(String[] args) {
		Person robert = Person.makePerson("Robert", "Flushein", Gender.MALE, 2004);
		System.out.println(robert.show());
		
		System.out.println(robert.isAdult());
		
		Person robert2 = Person.makePerson("Robert", "Flushein", Gender.MALE, 2004);
		Person robert3 = Person.makePerson("Robert", "Wolfschance", Gender.MALE, 2004);
		
		System.out.println(robert.equals(robert2));
		System.out.println(robert.equals(robert3));
	}
}
