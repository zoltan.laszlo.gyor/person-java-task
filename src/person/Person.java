package person;

import java.lang.String;
import java.util.Calendar;

public class Person {
	private String firstName;
	private String lastName;
	private Gender sex;
	private int birthYear;
	
	public Person() {
		
	}
	
	public boolean equals(Person person) {
		if(this.firstName.equals(person.firstName)
		&& this.lastName.equals(person.lastName)
		&& this.sex.equals(person.sex)
		&& this.birthYear == person.birthYear) {
			return true;
		}
		return false;
	}
	
	public boolean isAdult(){
		if(Calendar.getInstance().get(Calendar.YEAR) - this.birthYear <= 17) {
			return false;
		}
		return true;
	}
	
	public String show() {
		return this.firstName + " " + this.lastName + " " + this.birthYear + " " + this.sex.name();
	}
	
	public static Person makePerson(String firstName, String lastName, Gender sex, int birthYear) {
		if(isValid(lastName, lastName, sex, birthYear)) {
			Person example = new Person();
			example.setBirthYear(birthYear);
			example.setFirstName(firstName);
			example.setLastName(lastName);
			example.setSex(sex);	
			return example;
		}
		return null;
		
	}
	
	private static boolean isValid(String firstName, String lastName, Gender sex, int birthYear) {
		if(firstName != null && lastName != null
		&& firstName.length() >= 2 && lastName.length() >= 2
		&& Character.isUpperCase(firstName.charAt(0)) 
		&& Character.isUpperCase(lastName.charAt(0))
		&& birthYear >= 1880 && birthYear <= 2019
		) {
			return true;
		}
		return false;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Gender getSex() {
		return sex;
	}

	public void setSex(Gender sex) {
		this.sex = sex;
	}

	public int getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(int birthYear) {
		this.birthYear = birthYear;
	} 
}
